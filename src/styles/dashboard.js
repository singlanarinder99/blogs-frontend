import styled from "styled-components";
import { UserOutlined } from "@ant-design/icons";

const HeaderContainer = styled.header`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: 1rem;
  background-color: #001529;
  color: white;
`;

const Logo = styled.div`
  width: 120px;
  height: 31px;
  background: rgba(255, 255, 255, 0.2);
  margin-right: 1rem;
`;

const Menu = styled.ul`
  display: flex;
  list-style: none;
  margin: 0;
  padding: 0;
`;

const MenuItem = styled.li`
  margin-right: 1rem;
  cursor: pointer;
`;

const ProfileIcon = styled(UserOutlined)`
  font-size: 24px;
  color: white;
  cursor: pointer;
`;

const BlogSection = styled.div`
  padding: 50px;
  .postBlogButton {
    display: flex;
    justify-content: right;
  }
`;
const BlogCard = styled.div`
  background: #eee;
  padding: 10px;
  margin-top: 22px;
  border-radius: 16px;
  h1 {
    margin: 0;
    font-size: 25px;
  }
`;

const ProfileSection = styled.div`
  display: grid;
  justify-content: center;
  justify-items: center;
  margin-top: 82px;
  .image {
  }
  section {
    width: 350px;
    background: #eee;
    height: 350px;
    padding: 10px;
    border-radius: 20px;
    position: relative;
  }
  div {
    margin-top: 10px;
  }
  .button {
    padding: 0.5rem 1rem;
    margin: 0.5rem;
    background-color: #007bff;
    color: white;
    border: none;
    cursor: pointer;
    position: absolute;
    bottom: 0;
    left: 37%;

    &:hover {
      background-color: #0056b3;
    }
  }
`;

export {
  ProfileIcon,
  MenuItem,
  Menu,
  Logo,
  HeaderContainer,
  BlogSection,
  BlogCard,
  ProfileSection,
};
