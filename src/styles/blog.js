import styled from "styled-components";

const BlogHeader = styled.div`
  background: #ccc5c5;
  height: 100px;
  justify-content: center;
  display: flex;
  align-items: center;
  font-weight: bolder;
  position: relative;
  font-size: 12px;
  span {
    position: absolute;
    right: 11px;
    bottom: 7px;
    color: #716f6f;
  }
`;

const BlogSection = styled.div`
  padding-left: 20px;
  color: black;
  .editBlog {
    display: flex;
    justify-content: right;
    margin-right: 25px;
  }
`;

export { BlogHeader, BlogSection };
