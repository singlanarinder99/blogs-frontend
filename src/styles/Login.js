import styled from "styled-components";

const LoginForm = styled.form`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin-top: 100px;
  background: #eee;
  height: 500px;
  border-radius: 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  .inputBox {
    margin-top: 20px;
  }
  label {
    font-weight: bolder;
    font-size: 15px;
    color: #605b5b;
  }
  textarea {
    width: 300px;
    margin-top: 6px;
    padding: 0.5rem;
  }
`;

const InputField = styled.input`
  padding: 0.5rem;
  width: 300px;
  margin-top: 6px;
`;

const SubmitButton = styled.button`
  padding: 0.5rem 1rem;
  /* margin: 0.5rem; */
  background-color: #007bff;
  color: white;
  border: none;
  cursor: pointer;
  margin-top: 45px;
  margin-bottom: 5px;
  &:hover {
    background-color: #0056b3;
  }
`;
const SignUpButton = styled.button`
  padding: 0.5rem 1rem;
  /* margin: 0.5rem; */
  background-color: #007bff;
  color: white;
  border: none;
  cursor: pointer;
  margin-top: 10px;
  &:hover {
    background-color: #0056b3;
  }
`;

const ErrorBox = styled.p`
  color: red;
  margin-top: 5px;
  font-size: 14px;
`;

const LoginSection = styled.div`
  padding: 0 200px;
  h2 {
    text-align: center;
  }
`;

export {
  LoginForm,
  InputField,
  SubmitButton,
  ErrorBox,
  LoginSection,
  SignUpButton,
};
