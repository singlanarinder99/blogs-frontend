import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./pages/login";
import SignUp from "./pages/sign-up";
import Dashboard from "./pages/dashboard";
import Profile from "./pages/profle";
import Blog from "./pages/blogs";
import PostBlog from "./pages/post-blog";
import EditBlog from "./pages/post-blog/edit-blog";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/sign-up" element={<SignUp />} />
        <Route path="/dashboard" element={<Dashboard />} />
        <Route path="/profile" element={<Profile />} />
        <Route path="/blog/:id" element={<Blog />} />
        <Route path="/create-blog" element={<PostBlog />} />
        <Route path="/edit-blog" element={<EditBlog />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
