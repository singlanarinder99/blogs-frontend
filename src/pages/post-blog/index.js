import React, { useState } from "react";
import axios from "axios";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import Loader from "../../components/Loader";
import Header from "../../components/Header";
import {
  LoginForm,
  InputField,
  SubmitButton,
  ErrorBox,
  LoginSection,
} from "../../styles/Login";

const CreateEditBlog = () => {
  const navigate = useNavigate();
  const [loader, setLoader] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    console.log("data", data);
    const token = localStorage.getItem("token");
    setLoader(true);
    axios
      .post("http://localhost:5001/api/blogs", data, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      })
      .then((res) => {
        setLoader(false);
        alert("Blog Created successfully");
        navigate("/dashboard");
      })
      .catch((err) => {
        setLoader(false);
        alert(err?.response?.data);
      });
  };

  return (
    <div>
      {loader && <Loader />}
      <Header />
      <LoginSection>
        <h2>Post Blog</h2>
        <LoginForm onSubmit={handleSubmit(onSubmit)}>
          <div className="inputBox">
            <div>
              <label>Heading:</label>
            </div>
            <InputField
              type="text"
              placeholder="heading"
              {...register("heading", {
                required: "This field is required",
              })}
            />
            {errors.heading && <ErrorBox>{errors.heading.message}</ErrorBox>}
          </div>

          <div className="inputBox">
            <div>
              <label>Body:</label>
            </div>
            <textarea
              type="text"
              cols={5}
              rows={5}
              placeholder="body"
              {...register("body", {
                required: "This field is required",
              })}
            />
            {errors.body && <ErrorBox>{errors.body.message}</ErrorBox>}
          </div>
          <SubmitButton type="submit">Submit</SubmitButton>
        </LoginForm>
      </LoginSection>
    </div>
  );
};

export default CreateEditBlog;
