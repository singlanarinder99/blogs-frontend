import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import Header from "../../components/Header";
import { BlogSection, BlogCard } from "../../styles/dashboard";
import { SubmitButton } from "../../styles/Login";

const Dashboard = () => {
  const navigate = useNavigate();
  const [blogs, setBlogs] = useState([]);

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      axios
        .get("http://localhost:5001/api/blogs", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          console.log(res?.data);
          setBlogs(res?.data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, []);

  return (
    <div>
      <Header />

      <BlogSection>
        <div className="postBlogButton">
          <SubmitButton onClick={() => navigate("/create-blog")}>
            Post New Blog
          </SubmitButton>
        </div>
        {blogs?.map((item, index) => {
          const truncatedBody =
            item.body.length > 50
              ? item.body.substring(0, 50) + "..."
              : item.body;
          return (
            <BlogCard key={`blog${index}`}>
              <h1>{item?.heading}</h1>
              <p>{truncatedBody}</p>
              <SubmitButton
                onClick={() =>
                  navigate(`/blog/${item._id}`, { state: { blog: item } })
                }
              >
                View Blog
              </SubmitButton>
            </BlogCard>
          );
        })}
      </BlogSection>
    </div>
  );
};

export default Dashboard;
