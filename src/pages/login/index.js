import React, { useState } from "react";
import axios from "axios";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import Loader from "../../components/Loader";
import {
  LoginForm,
  InputField,
  SubmitButton,
  ErrorBox,
  LoginSection,
  SignUpButton,
} from "../../styles/Login";

const Login = () => {
  const navigate = useNavigate();
  const [loader, setLoader] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (data) => {
    setLoader(true);
    axios
      .post("http://localhost:5001/api/login", data)
      .then((res) => {
        setLoader(false);
        console.log(res?.data);
        localStorage.setItem("token", res?.data?.token);
        navigate("/dashboard");
      })
      .catch((err) => {
        setLoader(false);
        alert(err?.response?.data?.error);
        console.log(err);
      });
  };

  return (
    <div>
      {loader && <Loader />}
      <LoginSection>
        <h2>Login</h2>
        <LoginForm onSubmit={handleSubmit(onSubmit)}>
          <div className="inputBox">
            <div>
              <label>Email:</label>
            </div>
            <InputField
              type="email"
              placeholder="Email"
              {...register("email", {
                required: "Email is required",
                pattern: {
                  value: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/i,
                  message: "Invalid email address",
                },
              })}
            />
            {errors.email && <ErrorBox>{errors.email.message}</ErrorBox>}
          </div>
          <div className="inputBox">
            <div>
              <label>Password:</label>
            </div>
            <InputField
              type="password"
              placeholder="Password"
              {...register("password", {
                required: "Password is required",
                minLength: {
                  value: 4,
                  message: "Password must be at least 4 characters",
                },
              })}
            />
            {errors.password && <ErrorBox>{errors.password.message}</ErrorBox>}
          </div>
          <SubmitButton type="submit">Login</SubmitButton>
          <span>Or</span>
          <SignUpButton onClick={() => navigate("/sign-up")} type="button">
            Signup
          </SignUpButton>
        </LoginForm>
      </LoginSection>
    </div>
  );
};

export default Login;
