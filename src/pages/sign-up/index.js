import React, { useState } from "react";
import axios from "axios";
import { useForm } from "react-hook-form";
import { useNavigate } from "react-router-dom";
import Loader from "../../components/Loader";
import {
  LoginForm,
  InputField,
  SubmitButton,
  ErrorBox,
  LoginSection,
  SignUpButton,
} from "../../styles/Login";

const Login = () => {
  const navigate = useNavigate();
  const [loader, setLoader] = useState(false);
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();

  const onSubmit = (formData) => {
    // setLoader(true);
    console.log("data", formData);
    const data = new FormData();

    Object.keys(formData).forEach((key) => {
      if (key !== "profilePicture") {
        data.append(key, formData[key]);
      }
    });

    const files = formData["profilePicture"];
    for (let i = 0; i < files.length; i++) {
      data.append("profilePicture", files[i]);
    }

    axios
      .post("http://localhost:5001/api/users", data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      })
      .then((res) => {
        setLoader(false);
        console.log(res?.data);
        alert(res?.data?.msg);
        navigate("/");
      })
      .catch((err) => {
        setLoader(false);
        alert(err?.response?.data?.error);
      });
  };

  return (
    <div>
      {loader && <Loader />}
      <LoginSection>
        <h2>Create Account</h2>
        <LoginForm onSubmit={handleSubmit(onSubmit)}>
          <div className="inputBox">
            <div>
              <label>Name:</label>
            </div>
            <InputField
              type="text"
              placeholder="name"
              {...register("name", {
                required: "name is required",
              })}
            />
            {errors.name && <ErrorBox>{errors.name.message}</ErrorBox>}
          </div>
          <div className="inputBox">
            <div>
              <label>Email:</label>
            </div>
            <InputField
              type="email"
              placeholder="Email"
              {...register("email", {
                required: "Email is required",
                pattern: {
                  value: /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/i,
                  message: "Invalid email address",
                },
              })}
            />
            {errors.email && <ErrorBox>{errors.email.message}</ErrorBox>}
          </div>
          <div className="inputBox">
            <div>
              <label>Password:</label>
            </div>
            <InputField
              type="password"
              placeholder="Password"
              {...register("password", {
                required: "Password is required",
                minLength: {
                  value: 4,
                  message: "Password must be at least 4 characters",
                },
              })}
            />
            {errors.password && <ErrorBox>{errors.password.message}</ErrorBox>}
          </div>
          <div className="inputBox">
            <div>
              <label>Password:</label>
            </div>
            <InputField
              type="file"
              {...register("profilePicture", {
                required: "file is required",
              })}
            />
            {errors.profilePicture && (
              <ErrorBox>{errors.profilePicture.message}</ErrorBox>
            )}
          </div>
          <SubmitButton type="submit">Create Account</SubmitButton>
          <span>Or</span>
          <SignUpButton onClick={() => navigate("/")} type="button">
            Login
          </SignUpButton>
        </LoginForm>
      </LoginSection>
    </div>
  );
};

export default Login;
