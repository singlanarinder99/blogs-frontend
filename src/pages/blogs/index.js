import React, { useEffect, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { SubmitButton } from "../../styles/Login";
import { BlogHeader, BlogSection } from "../../styles/blog";
import Header from "../../components/Header";

const Blog = () => {
  const navigate = useNavigate();
  const location = useLocation();
  const [blog, setBlog] = useState(null);

  useEffect(() => {
    if (location.state && location.state.blog) {
      console.log("location.state.blog", location.state.blog);
      setBlog(location.state.blog);
    }
  }, [location]);

  return (
    <div>
      <Header />
      <BlogHeader>
        {blog?.heading}
        <span>
          Posted By: {blog?.author?.name} on{" "}
          {new Date(blog?.postedDate).toLocaleDateString()}
        </span>
      </BlogHeader>

      <BlogSection>
        <div className="editBlog">
          <SubmitButton
            onClick={() => {
              navigate("/edit-blog", { state: { blogData: blog } });
            }}
          >
            Edit Blog
          </SubmitButton>
        </div>
        <p>{blog?.body}</p>
      </BlogSection>
    </div>
  );
};

export default Blog;
