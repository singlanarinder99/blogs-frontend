import React, { useEffect, useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router-dom";
import { ProfileSection } from "../../styles/dashboard";
import Header from "../../components/Header";

const Profile = () => {
  const navigate = useNavigate();
  const [data, setData] = useState();

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      axios
        .get("http://localhost:5001/api/user", {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        })
        .then((res) => {
          console.log(res?.data);
          setData(res?.data);
          // setBlogs(res?.data);
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }, []);

  return (
    <div>
      <Header />

      <ProfileSection>
        <div className="image">
          <img
            src={`data:image/;base64,${data?.profilePicture}`}
            alt="profile"
            style={{
              maxWidth: "100px",
              maxHeight: "100px",
              cursor: "pointer",
              borderRadius: "50%",
              width: "100px",
            }}
          />
        </div>

        <section>
          <div> User Name : {data?.name}</div>
          <div> Email : {data?.email}</div>

          <button
            onClick={() => {
              localStorage.clear("token");
              navigate("/");
            }}
            className="button"
          >
            Logout
          </button>
        </section>
      </ProfileSection>
    </div>
  );
};

export default Profile;
