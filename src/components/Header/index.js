import React from "react";
import { useNavigate } from "react-router-dom";
import {
  HeaderContainer,
  Menu,
  MenuItem,
  ProfileIcon,
} from "../../styles/dashboard";

const Header = () => {
  const navigate = useNavigate();
  return (
    <HeaderContainer>
      <Menu>
        <MenuItem onClick={() => navigate("/dashboard")}>Dashboard</MenuItem>
      </Menu>
      <ProfileIcon onClick={() => navigate("/profile")} />
    </HeaderContainer>
  );
};

export default Header;
