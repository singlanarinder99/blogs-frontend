import React from "react";
import { Spin } from "antd";

const Loader = () => {
  return (
    <div
      style={{
        display: "flex",
        position: "fixed",
        top: 0,
        bottom: 0,
        right: 0,
        left: 0,
        justifyContent: "center",
        alignItems: "center",
        zIndex: 99999,
        backgroundColor: "rgb(255, 255, 255, 0.5)",
      }}
    >
      <Spin size="large">
        <div className="content" />
      </Spin>
    </div>
  );
};

export default Loader;
